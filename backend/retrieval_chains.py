from typing import Any, List,Tuple
from langchain.chains import ConversationalRetrievalChain

def run_conversational_retrieval(
    chat,
    retriever,
    query:str,
    chat_history: List[Tuple[str,Any]]=[]) ->Any:

    qa = ConversationalRetrievalChain.from_llm(
        llm=chat,
        chain_type="stuff",
        retriever=retriever,
        return_source_documents=True)

    response = qa(
        {
         "question": query,
         "chat_history": chat_history
         }
        )

    print(response["answer"])
