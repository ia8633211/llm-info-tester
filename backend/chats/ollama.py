import os
from langchain_community.chat_models import ChatOllama


def get_ollama_chat(
    base_url: str = os.environ.get("OLLAMA_HOST"),
    model: str = os.environ.get("OLLAMA_MODEL"),
    temperature: float = 0) -> ChatOllama:

    chat = ChatOllama(
        base_url= base_url,
        model = model,
        temperature=temperature,
        verbose=True
    )

    return chat
