from langchain_openai import ChatOpenAI

def get_openai_chat()-> ChatOpenAI:
    chat = ChatOpenAI(
        verbose=True,
        temperature=0)

    return chat