from typing import List
from dotenv import load_dotenv
from llama_index import Document
from langchain.text_splitter import RecursiveCharacterTextSplitter
from backend.data import ragatouille_client, pinecone_client, weaviate_client

load_dotenv()

def split_documents(raw_documents: List[Document]):
    str_documents = RecursiveCharacterTextSplitter(chunk_size=600, chunk_overlap=50)
    documents = str_documents.split_documents(raw_documents)

    return documents

def ingest_into_ragatouille(raw_documents: List[Document], index_name:str=""):
    documents = split_documents(raw_documents=raw_documents)
    final_documents = [document.page_content for document in documents]

    ragatouille_client.ingest_data(
        str_documents=final_documents,
        index_name=index_name)

def ingest_into_pinecone(raw_documents: List[Document], index_name:str=""):
    documents = split_documents(raw_documents=raw_documents)

    pinecone_client.ingest_data(
        documents=documents,
        index_name=index_name
    )

def ingest_into_weaviate(raw_documents: List[Document], index_name:str=""):
    documents = split_documents(raw_documents=raw_documents)

    retriever = weaviate_client.ingest_data(
        documents=documents,
        index_name=index_name
    )

    return retriever
