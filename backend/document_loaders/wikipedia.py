from typing import List
from langchain_community.document_loaders import WikipediaLoader
from llama_index import Document

def load_wikipedia(query: str = "any")-> List[Document]:
    loader = WikipediaLoader(query=query, load_max_docs=10)
    
    raw_documents = loader.load()
    
    return raw_documents
