from typing import List
from langchain_community.document_loaders import ReadTheDocsLoader
from llama_index import Document

def load_rtd(path)-> List[Document]:
    loader = ReadTheDocsLoader(path)

    raw_documents = loader.load()
    
    return raw_documents
