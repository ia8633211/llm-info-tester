from typing import List
from ragatouille import RAGPretrainedModel


def ingest_data(str_documents:List[str], index_name: str, max_document_length=180):
    rag = RAGPretrainedModel.from_pretrained("colbert-ir/colbertv2.0")

    rag.index(
    collection=str_documents,
    index_name=index_name,
    max_document_length=max_document_length,
    split_documents=True,
    )


def get_retriever(index:str=""):
    index_path = f".ragatouille/colbert/indexes/{index}"
    retriever = RAGPretrainedModel.from_index(index_path).as_langchain_retriever()

    return retriever
