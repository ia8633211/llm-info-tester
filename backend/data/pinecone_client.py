import os
from pinecone import Pinecone
from typing import List
from langchain_community.vectorstores.pinecone import Pinecone as PineconeLangchain
from langchain_openai import OpenAIEmbeddings
from llama_index import Document

def preconfigure():
    pc = Pinecone(api_key=os.environ["PINECONE_API_KEY"])
    embeddings = OpenAIEmbeddings(model="text-embedding-3-small")

    return embeddings, pc


def ingest_data(
    documents: List[Document],
    index_name: str):

    embeddings, pc = preconfigure()
    PineconeLangchain.from_documents(documents, embeddings, index_name=index_name)


def get_retriever(
    index_name: str):
    embeddings, pc = preconfigure()
    docsearch = PineconeLangchain.from_existing_index(
        index_name=index_name,
        embedding=embeddings,
        )

    return docsearch.as_retriever()
