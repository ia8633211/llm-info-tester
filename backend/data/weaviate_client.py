import os
from typing import List
from langchain_openai import OpenAIEmbeddings
from llama_index import Document
import weaviate
from langchain_community.vectorstores.weaviate import Weaviate as WeaviateLangChain

def preconfigure():
    url = os.environ.get("WEAVIATE_HOST")

    client = weaviate.Client(
        url=url,
        additional_headers={
            "X-OpenAI-Api-Key": os.getenv("OPENAI_API_KEY"),
        })

    class_obj = {
        "class": "Document",
        "vectorizer": "text2vec-openai",
        "moduleConfig": {
        "text2vec-openai": {
            "vectorizeClassName": True
            }
        }
    }

    try:
        client.schema.create_class(class_obj)
    except:
        pass
    finally:
        embeddings = OpenAIEmbeddings(model="text-embedding-3-small")


    return embeddings, client

def ingest_data(
    documents: List[Document],
    index_name: str):

    embeddings, client = preconfigure()
    db = WeaviateLangChain.from_documents(
        client=client,
        documents=documents,
        embedding=embeddings,
        text_key="text",
        by_text=False,
        index_name=index_name
    )

    return db.as_retriever()

def get_retriever(index_name: str):
    embeddings, client = preconfigure()
    db = WeaviateLangChain(
        client=client,
        embedding=embeddings,
        text_key="text",
        by_text=False,
        index_name="Document"
        )

    return db.as_retriever()