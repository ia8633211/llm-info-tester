import time
import streamlit as st
from dotenv import load_dotenv
from streamlit_chat import message
from backend.chats.ollama import get_ollama_chat
from backend.chats.openai import get_openai_chat
from backend.data import pinecone_client, ragatouille_client, weaviate_client
from backend.document_loaders import wikipedia
from backend.ingesters import ingest_into_pinecone, ingest_into_ragatouille, ingest_into_weaviate
from backend.retrieval_chains import run_conversational_retrieval

load_dotenv()

def retrieve_info():
    print("Getting info from wikipedia about pokemon")
    raw_documents = wikipedia.load_wikipedia(query="pokemon")

    return raw_documents

def check_history():
    if "user_prompt_history" not in st.session_state:
        st.session_state["user_prompt_history"] = []
    if "chat_answers_history" not in st.session_state:
        st.session_state["chat_answers_history"] = []

def get_chat(chat: str = "openai"):
    if chat == "ollama":
        return get_ollama_chat()

    return get_openai_chat()

def run_chatbot():
    st.header("Bienvenido al info tester")

    prompt = st.text_input("Prompt", placeholder="Enter your prompt here...")

    check_history()

    if prompt:
        with st.spinner("Generating response..."):
            generated_response = run_conversational_retrieval(prompt)    
        sources = set([doc.metadata["source"] for doc in generated_response["source_documents"]])
        
        formatted_response = f"{generated_response['answer']}"

        st.session_state["user_prompt_history"].append(prompt)
        
        st.session_state["chat_answers_history"].append(formatted_response)

        if st.session_state["chat_answers_history"]:
            for generated_response, user_query in zip(
                st.session_state["chat_answers_history"],
                st.session_state["user_prompt_history"]):
                message(user_query, is_user=True)
                message(generated_response)

def run_ragatouille(input: str = ""):
    index_name = "default"

    raw_documents = retrieve_info()
    print("Information retrieved and stored, going to check the question")
    print(f"Question -> {input}")

    ingest_into_ragatouille(raw_documents=raw_documents, index_name=index_name)
    retriever = ragatouille_client.get_retriever(index=index_name)
    chat = get_chat()
    run_conversational_retrieval(
        chat=chat,
        retriever=retriever,
        query=input,
        chat_history=[]
    )

def run_pinecone(input: str = ""):
    index_name = "default"

    raw_documents = retrieve_info()
    print("Information retrieved and stored, going to check the question")
    print("Question -> {}".format(input))

    ingest_into_pinecone(raw_documents=raw_documents, index_name=index_name)
    retriever = pinecone_client.get_retriever(index_name=index_name)
    chat = get_chat()

    run_conversational_retrieval(
        chat=chat,
        retriever=retriever,
        query=input,
        chat_history=[]
    )

def run_weaviate(input: str = "", chat: str = ""):
    index_name = ""
    raw_documents = retrieve_info()
    print("Information retrieved and stored, going to check the question")
    print(f"Question -> {input}")
    ingest_into_weaviate(raw_documents=raw_documents, index_name=index_name)
    retriever = weaviate_client.get_retriever(index_name=index_name)
    chat = get_chat()

    run_conversational_retrieval(
        chat=chat,
        retriever=retriever,
        query=input,
        chat_history=[]
    )

if __name__ == "__main__":
    INPUT = "Tell me 10 facts about Pikachu"

    print("Checking performance of multiple techniques for the same model")

    print("Weaviate")
    start_weaviate = time.time()
    run_weaviate(INPUT)
    end_weaviate = time.time()
    weaviate_time = end_weaviate - start_weaviate
    print(weaviate_time)

    print("Using Pinecone")
    start_pinecone = time.time()
    run_pinecone(INPUT)
    end_pinecone = time.time()
    pinecone_time = end_pinecone - start_pinecone
    print(pinecone_time)

    print("Using RAGautouille")
    start_ragatouille = time.time()
    run_ragatouille(INPUT)
    end_ragatouille = time.time()
    ragatouille_time = end_ragatouille - start_ragatouille
    print(ragatouille_time)

    print("TIMES")
    print(f"Using Weaviate     -> {weaviate_time} seconds")
    print(f"Using Pinecone     -> {pinecone_time} seconds")
    print(f"Using RAGattouille -> {ragatouille_time} seconds")
