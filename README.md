# Langchain Info Tester

Este proyecto es un conjunto de herramientas para probar la calidad y la precisión de la información proporcionada por Langchain (Large Language Model).

## Instalación

Para utilizar este proyecto, sigue estos pasos:

1. Clona el repositorio en tu máquina local:

```
git clone https://gitlab.com/ia8633211/llm-info-tester.git
```

2. Instala las dependencias utilizando pip:

```
pip install -r requirements.txt
```

## Uso

Este repositorio realiza varias pruebas sobre Langchain, e irá aumentando su funcionalidad a lo largo del tiempo, observa el comportamiento del archivo main.py para saber que está realizando

Para usar este proyecto, sigue los siguientes pasos:
1. Ejecuta el docker-compose

```
docker compose up -d
```
Mira el contenido para conocer que se va a desplegar

2. Ejecuta el script principal `main.py`:

```
python main.py
```

## Dependencias

- Docker
- docker compose plugin
- Python 3.x
- Bibliotecas especificadas en `requirements.txt`

## Licencia

Este proyecto está bajo la Licencia MIT. Consulta el archivo `LICENSE` para más detalles.
